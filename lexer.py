from token1 import *
from symbol import *
from error import *

class Lexer:
    def __init__(self, text):
        self.text = text
        self.reset()
    def reset(self):
        self.pos = 0
        self.current_char = self.text[self.pos]
        self.position = 0
        self.line = 1
    def advance(self):
        self.pos += 1
        self.position += 1
        self.current_char = None if self.pos > len(self.text) - 1 else self.text[self.pos]
    def skip_whitespace(self):
        while self.current_char is not None and self.current_char.isspace():
            if self.current_char == '\n':
                self.line += 1
                self.position = 0
            self.advance()
    def integer(self):
        digit = ""
        while self.current_char.isdigit():
            digit += self.current_char
            self.advance()
        return digit
    def string(self):
        str = ""
        while self.current_char is not None and self.current_char.isalpha():
            str += self.current_char
            self.advance()
        return str
    def comment(self):
        c = ''
        while self.current_char != Symbol.RIGHT_CURL:            
            c += self.current_char
            self.advance()
            self.skip_whitespace()
        self.advance()
        return c
    def string_literal(self):
        literal = ""
        while self.current_char != '"':
            literal += self.current_char
            self.advance()
        self.advance()
        return literal
    def __next_token(self):
        while self.current_char is not None:    
            if self.current_char.isspace():
                self.skip_whitespace()
                continue
            if self.current_char.isalpha():
                stri = self.string()
                token_type = ""
                import html
                if stri.upper() in RESERVED_KEYWORDS:
                    token_type = TokenType.KEYWORD
                # if stri.title() in dir(html):
                #     token_type = TokenType.WEBOBJECT
                else:
                    token_type = TokenType.IDENTIFIER
                return Token(token_type, stri)
            if self.current_char.isdigit():
                digit = self.integer()
                return Token(TokenType.INTEGER, digit)
            if self.current_char == Symbol.DOUBLE_QUOTE:
                self.advance()
                return Token(TokenType.STRING, self.string_literal())
            if self.current_char == Symbol.LEFT_CURL:
                self.advance()
                self.comment()
                continue
            if self.current_char == Symbol.PERIOD:
                self.advance()
                return Token(TokenType.EOS, Symbol.PERIOD)
            if self.current_char == Symbol.EQUAL:
                self.advance()
                return Token(TokenType.OPERATOR, Symbol.EQUAL)
            if self.current_char == Symbol.COLON:
                self.advance()
                return Token(TokenType.SYMBOL, Symbol.COLON)
            if self.current_char == Symbol.LEFT_PAREN:
                self.advance()
                return Token(TokenType.SYMBOL, Symbol.LEFT_PAREN)
            if self.current_char == Symbol.RIGHT_PAREN:
                self.advance()
                return Token(TokenType.SYMBOL, Symbol.RIGHT_PAREN)
            if self.current_char == Symbol.COMMA:
                self.advance()
                return Token(TokenType.SYMBOL, Symbol.COMMA)
            if self.current_char == Symbol.SEMI_COLON:
                self.advance()
                return Token(TokenType.SYMBOL, Symbol.SEMI_COLON)
            if self.current_char == Symbol.OPEN_BRACKET:
                self.advance()
                return Token(TokenType.SYMBOL, Symbol.OPEN_BRACKET)
            if self.current_char == Symbol.CLOSE_BRACKET:
                self.advance()
                return Token(TokenType.SYMBOL, Symbol.CLOSE_BRACKET)
            Error("Invalid character '" + self.current_char + "' at line " + str(self.line) + " position " + str(self.position))
        return Token(TokenType.EOF, None)
    def get_next_token(self):
        token = self.__next_token()
        return token

#!/usr/bin/python
class ParseTree:
    pass

class Server(ParseTree):
    def __init__(self):
        self.statements = []

class Page(ParseTree):
    def __init__(self):
        self.statements = []

class Variable(ParseTree):
    def __init__(self, name, props):
        self.name = name
        self.properties = props

class PropertyStatement(ParseTree):
    def __init__(self, command, args):
        self.command = command
        self.args = args

class Statement(ParseTree):
    def __init__(self, command, args):
        self.command = command
        self.args = args
        self.subs = []
        self.ctx = None

class IfStatement(ParseTree):
    def __init__(self, left, rel, right):
        self.left = left
        self.rel = rel
        self.right = right
        self.subs = []

class OnStatement(ParseTree):
    def __init__(self, event):
        self.event = event
        self.subs = []

class WebDocument(ParseTree):
    def __init__(self):
        self.pages = []

class Nothing(ParseTree):
    pass

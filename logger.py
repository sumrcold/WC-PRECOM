#!/usr/bin/python
class Logger:
    @staticmethod
    def warning(text):
        Logger.log("\033[0;33m[WARNING]:", text)

    @staticmethod
    def debug(text):
        Logger.log("\033[0m[ DEBUG ]:", text)

    @staticmethod
    def error(text):
        Logger.log("\033[0;31m[ ERROR ]:", text)

    @staticmethod
    def verbose(text):
        Logger.log("\033[0;37m[VERBOSE]:", text)

    @staticmethod
    def log(flag, text):
        print(flag + " " + str(text) + "\033[0;37m")

class Token:
    def __init__(self, type, value):
        self.type = type
        self.value = value
    def __str__(self):
        return "Token({type}, {value})".format(type=self.type, value=self.value)
    def __repr__(self):
        return self.__str__()

class TokenType:
    IDENTIFIER = "IDENTIFIER"
    KEYWORD = "KEYWORD"
    STRING = "STRING"
    INTEGER = "INTEGER"
    SYMBOL = "SYMBOL"
    OPERATOR = "OPERATOR"
    EOF = "EOF"
    EOS = "EOS"
    COMMAND = "COMMAND"
    WEBOBJECT = "WEBOBJECT"


class Keyword:
    NEW = "NEW"
    TASK = "TASK"
    IF = "IF"
    ELSE = "ELSE"
    THEN = "THEN"
    ON = "ON"
    CREATE = 'CREATE'
    PAGE = 'PAGE'
    TEXTFIELD = "TEXTFIELD"
    ADD = "ADD"
    REMOVE = "REMOVE"
    SECTION = "SECTION"
    FORM = "FORM"
    BUTTON = "BUTTON"
    INSERT = "INSERT"
    BREAK = "BREAK"
    IS = "IS"

class Command:
    CREATE = Keyword.CREATE
    ADD = Keyword.ADD
    REMOVE = Keyword.REMOVE
    INSERT = Keyword.INSERT

WEBOBJECTS = {Keyword.PAGE: TokenType.WEBOBJECT, \
Keyword.TEXTFIELD: TokenType.WEBOBJECT, \
Keyword.SECTION: TokenType.WEBOBJECT, \
Keyword.BUTTON: TokenType.WEBOBJECT, \
Keyword.FORM: TokenType.WEBOBJECT}
RESERVED_KEYWORDS = {"IF" : "IF", "WHILE" : "WHILE", "ON" : "ON", "IS" : "IS", "THEN": "THEN", "BREAK" : "BREAK", "AND" : "AND", "OR" : "OR"}
    
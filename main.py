#!/usr/bin/python
import time
import sys
import os
from logger import Logger
from tokenparser import TokenParser
from lexer import Lexer
from token1 import TokenType
from interpreter import Interpreter

START_TIME = time.time()
if __name__ == "__main__":
    ARGV = sys.argv
    if len(ARGV) > 1:
        import os.path
        if os.path.isfile(ARGV[1]):
            FILE = ARGV[1]
            FILENAME = os.path.splitext(FILE)[0]
            try:
                Logger.verbose("Reading file " + FILE + " ...")
                WCFILE = open(FILE, "r")
            except IOError:
                Logger.error("Unable to read file " + FILE)
                sys.exit()

            lexer = Lexer(WCFILE.read())
            Logger.debug("==Tokenization==")
            current_token = lexer.get_next_token()
            while current_token.type != TokenType.EOF:
                Logger.debug(str(current_token))
                current_token = lexer.get_next_token()
            lexer.reset()

            tokenparser = TokenParser(lexer)
            Logger.debug("==Output==")

            import os
            OUTPUT_DIR = "output"
            if not os.path.isdir(OUTPUT_DIR):
                os.mkdir(OUTPUT_DIR)
            
            INTERPRETER = Interpreter(tokenparser)
            INTERPRETER.interpret()
            WCFILE.close()
            
            
            
            Logger.verbose("Finished reading file " + FILENAME)
            Logger.verbose("Execution time = " + str(time.time() - START_TIME) + " sec(s)")
        else:
            Logger.error("File '" + ARGV[1] + "' does not exists")

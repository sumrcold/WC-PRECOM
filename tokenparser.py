from lexer import Lexer
from token1 import *
from error import *
from parsetree import *
from symbol import Symbol
import html

class TokenParser:
    def __init__(self, lexer):
        self.lexer = lexer
        self.current_token = self.lexer.get_next_token()
    def reset(self):
        self.lexer.reset()
        self.current_token = self.lexer.get_next_token()
    def eat(self, token_type):
        if self.current_token.type == token_type:
            self.current_token = self.lexer.get_next_token()
        else:
            self.__error()
    def __error(self):
        Error("Syntax error at line " + str(self.lexer.line) + " position " + str(self.lexer.position - len(self.current_token.value)) + " " + str(self.current_token))
    def nothing(self):
        return Nothing()
    def arguments(self):
        args = []
        while self.current_token.type == TokenType.IDENTIFIER or self.current_token.type == TokenType.STRING or self.current_token.type == TokenType.INTEGER:
            idr = self.current_token.value
            self.eat(self.current_token.type)
            if self.current_token.value == Symbol.LEFT_PAREN:
                self.eat(TokenType.SYMBOL)
                props = self.statement_list()
                self.eat(TokenType.SYMBOL)
                idr = Variable(idr, props)
                args.append(idr)
                break
            args.append(idr)
        return args
    def ifstatement(self):
        self.eat(TokenType.KEYWORD)
        left = self.reference()
        rel = self.current_token.value
        self.eat(TokenType.KEYWORD)
        right = self.current_token.value
        if self.current_token.type == TokenType.IDENTIFIER:
            self.eat(TokenType.IDENTIFIER)
        elif self.current_token.type == TokenType.STRING:
            right = "\"" + right + "\""
            self.eat(TokenType.STRING)
        self.eat(TokenType.KEYWORD)
        self.eat(TokenType.SYMBOL)
        ifs = IfStatement(left, rel, right)
        ifs.subs += [self.statement()]
        return ifs
    def reference(self):
        self.eat(TokenType.SYMBOL)
        idr = self.current_token.value
        self.eat(TokenType.IDENTIFIER)
        self.eat(TokenType.SYMBOL)
        return idr
    def onstatement(self):
        self.eat(TokenType.KEYWORD)
        s_node = OnStatement(self.current_token.value)        
        self.eat(TokenType.IDENTIFIER)
        self.eat(TokenType.SYMBOL)
        ctx = ""
        if self.current_token.value == Symbol.OPEN_BRACKET:
            ctx = self.reference()
        while self.current_token.type == TokenType.KEYWORD:
            if self.current_token.value.upper() == Keyword.IF:
                s_node.subs += [self.ifstatement()]
            self.eat(TokenType.KEYWORD)
        command = self.current_token.value
        self.eat(TokenType.IDENTIFIER)
        s = Statement(command, self.arguments())
        s.ctx = ctx
        s_node.subs += [s] 
        return s_node
    def statement(self):
        if self.current_token.type == TokenType.IDENTIFIER:
            command = self.current_token.value
            self.eat(TokenType.IDENTIFIER)
            s_node = Statement(command, self.arguments())
            self.eat(TokenType.EOS)
        elif self.current_token.type == TokenType.KEYWORD:
            key = self.current_token.value           
            if key.upper() == Keyword.IF:
                s_node = self.ifstatement()
            elif key.upper() == Keyword.ON:
                s_node = self.onstatement()
                self.eat(TokenType.EOS)
        else:
            s_node = self.nothing()
        return s_node
    def statement_list(self):
        sl_node = []
        while self.current_token.type == TokenType.COMMAND or self.current_token.type == TokenType.IDENTIFIER or self.current_token.type == TokenType.KEYWORD:
            sl_node.append(self.statement())
        return sl_node
    def page(self):
        statements = self.statement_list()
        page = Page()
        for statement in statements:
            page.statements.append(statement)
        return page
    def parse(self):
        wd_node = self.page()
        if self.current_token.type != TokenType.EOF:
            self.__error()
        return wd_node
    
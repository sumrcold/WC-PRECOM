class Symbol:
    PERIOD = '.'
    COMMA = ','
    DOUBLE_QUOTE = '"'
    OPEN_BRACKET = '['
    CLOSE_BRACKET = ']'
    COLON = ':'
    LEFT_PAREN = '('
    RIGHT_PAREN = ')'
    LEFT_CURL = '{'
    RIGHT_CURL = '}'
    EQUAL = '='
    COLON = ':'
    SEMI_COLON = ';'

class SymbolTable:
    def __init__(self):
        self.symbols = []
    def add_symbol(self, key, value):
        self.symbols += [{key : value}]
    def get_symbols(self):
        return self.symbols
    def size(self):
        return len(self.symbols)
        
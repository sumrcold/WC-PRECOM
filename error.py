from logger import Logger
import sys
class Error:
    def __init__(self, msg):
        self.msg = msg
        Logger.error(msg)        
        sys.exit(0)

class SyntaxError(Error):
    pass

class CompilerError(Error):
    pass
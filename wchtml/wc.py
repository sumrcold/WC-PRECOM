from view import View
from error import *
from stylesheet import Stylesheet
from style import Style
class WC:
    def __init__(self):
        self.view = None
        self.styles = None
        self.html = ""
        self.css = ""
        self.js = ""
        self.php = ""
        self.context = None
        self.name = ""
    def create(self, name, webobject):
        self.name =  name
        self.view = View(name)
        self.styles = Stylesheet()
        return self
    def add(self, name, webobject):
        self.view.append_child(webobject)
        self.styles.append_child(Style(webobject.node_name, None))
        return self.view
    # def text(self, t):
    #     self.view.body.text(t)
    def generate(self):
        if self.view is None:
            pass
        self.html = self.view.onload()
        self.css = self.view.load_style()
        self.js = self.view.load_script()
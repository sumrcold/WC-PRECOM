from wchtml.node import Node
from wchtml.jscript import JScript
import collections
class JEvent(Node):
    def __init__(self):
        super(JEvent, self).__init__()
        self.events = collections.OrderedDict()
    def __init_script(self, key):
        self.events[key] = JScript()
        return self.events[key]
    def onchange(self):
        return self.__init_script("onchange")
    def onclick(self):        
        return self.__init_script("onclick")
    def onkeydown(self):        
        return self.__init_script("onkeydown")
    def onmouseover(self):        
        return self.__init_script("onmouseover")
    def onmouseout(self):        
        return self.__init_script("onmouseout")
    def onload(self):        
        return self.__init_script("onload")
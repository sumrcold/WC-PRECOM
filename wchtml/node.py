#!/usr/bin/python
class Node:    
    def __init__(self):
        self.child_nodes = []
        self.attributes = None
        self.base_uri = None
        self.first_child = None
        self.next_sibling = None
        self.node_name = None
        self.node_type = None
        self.node_value = None
        self.owner_document = None
        self.parent_node = None
        self.previous_sibling = None
        self.text_content = None
    def append_child(self, node: "Node"):
        if node is None:
            return
        if self.first_child is None:
            self.first_child = node 
        node.parent_node = self
        self.child_nodes.append(node)
    def clone_node(self):
        clone = Node()
        clone.attributes = self.attributes
        clone.base_uri = self.base_uri
        clone.child_nodes = self.child_nodes
        clone.first_child = self.first_child
        clone.next_sibling = self.next_sibling
        clone.node_name = self.node_name
        clone.node_value = self.node_value
        clone.owner_document = self.owner_document
        clone.parent_node = self.parent_node
        clone.previous_sibling = self.previous_sibling
        clone.text_content = self.text_content
        return clone
    def has_attributes(self):
        pass
    def has_child_nodes(self):
        if self.child_nodes:
            return True
        return False
    def insert_before(self, newnode: "Node", existingnode: "Node"):
        i = 0
        cns = self.parent_node.child_nodes
        for node in cns:
            if node.is_same_node(existingnode):
                newnode.parent_node = self.parent_node
                cns.insert(i + 1, newnode)
                break
            i +=  1
        return newnode
    def is_equal_node(self, node: "Node"):
        return self.node_type == node.node_type and \
        self.node_name == node.node_name and \
        self.node_value == node.node_value and \
        self.child_nodes == node.child_nodes and \
        self.attributes == node.attributes
    def is_same_node(self, node: "Node"):
        return self == node
    def normalize(self):
        pass
    def remove_child(self, node: "Node"):
        i = self.child_nodes.index(node)
        rnode = self.child_nodes[i]
        del self.child_nodes[i]
        return rnode
    def replace_child(self, newnode: "Node", oldnode: "Node"):
        i = self.child_nodes.index(oldnode)
        self.child_nodes[i] = newnode
        return oldnode
        
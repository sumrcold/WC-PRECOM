from wchtml.element import Element
class Calendar(Element):
    def __init__(self):
        super(Calendar, self).__init__()
        self.node_name = "input"
        self.attrs['type'] = "date"
from wchtml.element import Element
class Textfield(Element):
    def __init__(self, content = ''):
        super(Textfield, self).__init__()
        self.content = content
        self.node_name = "input"
        self.attrs['type'] = "text"
    def autocomplete(self, v = "on"):
        self.attrs['autocomplete'] = v
    def hint(self, ph):
        self.attrs['placeholder'] = ph
    def value(self, v):
        self.attrs['value'] = v
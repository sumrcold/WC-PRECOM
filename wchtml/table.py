from wchtml.element import Element
class Td(Element):
     def __init__(self, content = ''):
        super(Td, self).__init__()
class Tr(Element):
     def __init__(self, content = ''):
        super(Tr, self).__init__()
class Table(Element):
    def __init__(self, content = ''):
        super(Table, self).__init__()
        self.__noc = 0
        self.__tc = 0
        self.__cr = None
    def columns(self, c):
        self.__noc = int(c)
    def add(self, name, element):      
        td = Td()
        td.append_child(element)     
        if self.__cr is None or self.__tc == self.__noc - 1:
            self.__cr = Tr()
            self.__tc = 0
            self.append_child(self.__cr)
        else:
            self.__tc += 1
        self.__cr.append_child(td)
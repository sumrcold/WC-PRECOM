from wchtml.element import Element
class Form(Element):
    def __init__(self):
        super(Form, self).__init__()
    def method(self, method):
        self.attrs['method'] = method
    def action(self, url):      
        self.attrs['action'] = url + ".php"
from wchtml.element import Element
class Script(Element):
    def __init__(self, content = ''):
        super(Script, self).__init__()
        self.attrs['type'] = "text/javascript"
    def type(self, t):
        self.attrs['type'] = t
    def src(self, s):
        self.attrs['src'] = s
  
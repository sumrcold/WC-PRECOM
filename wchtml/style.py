from wchtml.node import Node
import collections
class Style(Node):
    def __init__(self):
        super(Style, self).__init__()
        self.properties = collections.OrderedDict()
    def shadow(self, s, c = "#000000"):
        v = "inset 0 0 " + s + " " + c
        self.properties['-moz-box-shadow'] = v 
        self.properties['-webkit-box-shadow'] = v
        self.properties['box-shadow'] = v
    def alignment(self, v):
        self.properties['text-align'] = v
    def marginless(self):
        self.properties['margin'] = "0px"
    def borderless(self):
        self.properties['border'] = "0px"
    def position(self, v):
        self.properties['position'] = v
    def padding(self, d = "", v = ""):
        if d == "left":
            self.padding_left(v)
        elif d == "right":
            self.padding_right(v)
        elif d == "top":
            self.padding_top(v)
        elif d == "bottom":
            self.padding_bottom(v)
        else:
            self.properties["padding"] = d
    def padding_left(self, v):
        self.properties['padding-left'] = v
    def padding_right(self, v):
        self.properties['padding-right'] = v
    def padding_top(self, v):
        self.properties['padding-top'] = v
    def padding_bottom(self, v):
        self.properties['padding-bottom'] = v
    def background(self, b = "", v = ""):
        if b == "color":
            self.background_color(v)
    def background_color(self, color):
        self.properties['background-color'] = color
    def color(self, c):
        self.properties['color'] = c
    def font(self, f = "", v = ""):
        if f == "family":
            self.font_family(v)
        elif f == "size":
            self.font_size(v)
        else:
            self.properties['font'] = f
    def font_family(self, font):
        self.properties['font-family'] = font
    def font_size(self, s):
        self.properties['font-size'] = s
    def visibility(self, v):
        self.properties['visibility'] = v
    def width(self, w):
        self.properties['width'] = w
    def height(self, w):
        self.properties['height'] = w
class PScript:
    def __init__(self):
        self.__content = ""
    def add_statement(self, s):
        self.__content += "\n" + s + "\n" 
    def redirect(self, url):
        return "header(\"Location: " + url + "\");"
    def __str__(self):
        return self.__content
from wchtml.elementnode import ElementNode
class TextContent(ElementNode):
    def __init__(self):
        super(TextContent, self).__init__()
    def open_encode(self):
        return ""
    def __str__(self):
        return self.text_content
    def close_encode(self):
        return ""
    def onload(self):
        pass
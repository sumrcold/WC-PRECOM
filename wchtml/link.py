from wchtml.element import Element
class Link(Element):
    def __init__(self, content = ''):
        super(Link, self).__init__()
        self.attrs['rel'] = "stylesheet"
    def rel(self, r):
        self.attrs['rel'] = r
    def href(self, h):
        self.attrs['href'] = h
  
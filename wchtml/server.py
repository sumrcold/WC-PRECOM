from wchtml.wc import WC
from wchtml.pscript import PScript
class Server(WC):
    def __init__(self):
        super(Server, self).__init__()
        self.__content = ""
        self.vars = {}
    def database(self, host, username, password, database):
        print(host + " " + username + " " + password + " " + database)
    def add_statement(self, s):
        self.__content += s + "\n" 
    def catch(self, var):
        v = "$" + var
        self.vars[var] = v
        self.add_statement(v + " = $_POST['" + var + "'];")
    def redirect(self, url):
        self.add_statement("header(\"Location: " + url + "\");")
    def __str__(self):
        return "<?php\n" + self.__content + "?>"
    def generate(self):
        php = open("output/" + self.name + ".php", 'w')
        php.write(self.__str__())
        php.close()
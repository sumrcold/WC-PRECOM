from wchtml.textfield import Textfield
class Passwordfield(Textfield):
    def __init__(self, content = ''):
        super(Passwordfield, self).__init__()
        self.node_name = "input"
        self.attrs['type'] = "password"
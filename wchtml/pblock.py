class PBlock:
    def __init__(self):
        self.__content = ""
    def add_statement(self, stmt):
        self.__content += stmt + ";\n"
    def __str__(self):
        return "{\n" + self.__content + "}"
from wchtml.element import Element
class Checkbox(Element):
    def __init__(self):
        super(Checkbox, self).__init__()
        self.node_name = "input"
        self.attrs['type'] = "checkbox"
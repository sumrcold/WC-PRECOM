from wchtml.elementnode import ElementNode
from wchtml.textcontent import TextContent
from wchtml.style import Style
from wchtml.jevent import JEvent
import collections
class Element(ElementNode, Style, JEvent):
    def __init__(self):
        super(Element, self).__init__()   
        self.attrs = collections.OrderedDict()
        self.node_name = self.__class__.__name__.lower()
    def text(self, txt):
        if self.text_content is None:
            self.text_content = TextContent()
            self.append_child(self.text_content)
        if txt[0] == '(' and txt[len(txt) - 1] == ')':
            txt = str(eval(txt[1:len(txt) - 1]))
        self.text_content.text_content = txt
    def add(self, name, element):
        self.append_child(element)
        return element
    def center(self):
        import wchtml
        c = wchtml.center.Center()
        op = self.parent_node
        self.parent_node = c
        a = op.remove_child(self)
        c.append_child(a)
        op.append_child(c)
    def hidden(self):
        self.attrs["hidden"] = "hidden"
    def style_attr(self):
        style = ''
        if self.properties.items():
            style = ' style="'
        for (prop, val) in self.properties.items():
            style += prop + ':' + val + ';'
        if self.properties.items():
            style += '"'
        return style
    def open_encode(self):
        attrs = ''       
        for (attr, val) in self.attrs.items():
            attrs += " " + attr + '="' + val + '"'       
        #return "<" + self.node_name + attrs + self.style_attr() + ">"
        return "<" + self.node_name + attrs + ">"
    def close_encode(self):
        return "</" + self.node_name + ">"
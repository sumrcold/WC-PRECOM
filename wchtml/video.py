from wchtml.element import Element
class Video(Element):
    def __init__(self):
        super(Video, self).__init__()
    def autoplay(self, a = "autoplay"):
        self.attrs["autoplay"] = a
    def controls(self, c = "controls"):
        self.attrs["controls"] = c
    def height(self, h):
        self.attrs["height"] = h
    def loop(self, h):
        self.attrs["loop"] = h
    def muted(self, m = "muted"):
        self.attrs["muted"] = m
    def poster(self, p):
        self.attrs["poster"] = p
    def preload(self, p):
        self.attrs["preload"] = p
    def src(self, url):
        self.attrs["src"] = url
    def width(self, h):
        self.attrs["width"] = h   
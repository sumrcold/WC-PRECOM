from wchtml.element import Element
class Image(Element):
    def __init__(self):
        super(Image, self).__init__()
        self.node_name = "input"
        self.attrs['type'] = "image"
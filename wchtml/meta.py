from wchtml.element import Element
class Meta(Element):
    def __init__(self):
        super(Meta, self).__init__()
        self.attrs["charset"] = "UTF-8"
    def charset(self, c):
        self.attrs['charset'] = c
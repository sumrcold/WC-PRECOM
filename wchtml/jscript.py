class JScript:
    def __init__(self):
        self.__content = ""
        self.ctx = "this"
    def context(self, ctx = "this"):
        if ctx == '':
            return
        self.ctx = ctx
    def add_statement(self, s):
        self.__content += s + "\n" 
    def hint(h):
        return "placeholder = \"" + h + "\";"
    def text(msg):
        return "innerHTML = \"" + msg + "\";"
    def popup(msg, t = "alert"):
        return t + "('" + msg + "');"  
    def redirect(url):
        return "window.location = \"" + url + "\";"
    def __str__(self):
        return "function(){\n" + self.__content + "};"
    
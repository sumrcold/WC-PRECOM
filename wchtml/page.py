from wchtml.wc import WC
from wchtml.title import Title
class Page(WC):
    def __init__(self):
        super(Page, self).__init__()
        self.__title = None
    def title(self, t):
        if self.__title is None:
            self.__title = Title()
            self.view.head.append_child(self.__title)
        self.__title.text(t)